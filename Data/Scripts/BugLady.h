#pragma once
#include "GameObject.h"
ref class BugLady sealed : public GameObject
{
	public:
		void OnCollide(Rapid2DGameObject ^ go);
		void OnStart();
		void OnUpdate(int64 frameNumber);
		void OnRender();
		void OnDestroy();
		void OnClicked();
		void ResetBug();
		void SetHealth(int h);
		void DecHealth(int dmg);

	internal:

		BugLady(String ^ name, bool isDynamic, bool isPhysical, Rapid2DVector ^ position)
		: GameObject(name, isDynamic, isPhysical, position)
		{
	
			startEvent = m_go->mEvents->OnStart += ref new dgObjectStart(this, &BugLady::OnStart);
			updateEvent = m_go->mEvents->OnUpdate+= ref new dgObjectUpdate(this, &BugLady::OnUpdate);
			renderEvent = m_go->mEvents->OnObjectRender += ref new dgObjectRender(this, &BugLady::OnRender);
			collideEvent = m_go->mEvents->OnObjectCollide += ref new dgObjectCollide(this, &BugLady::OnCollide);
			destroyEvent = m_go->mEvents->OnObjectDestroy += ref new dgObjectDestroy(this, &BugLady::OnDestroy);
			clickedEvent = m_go->mEvents->OnObjectClicked += ref new dgObjectClicked(this, &BugLady::OnClicked);
			alive = true;
			deadtime = 0;
			speed = -0.050000f;
		};
	private:
		Windows::Foundation::EventRegistrationToken updateEvent;
		Windows::Foundation::EventRegistrationToken renderEvent;
		Windows::Foundation::EventRegistrationToken destroyEvent;
		Windows::Foundation::EventRegistrationToken collideEvent;
		Windows::Foundation::EventRegistrationToken startEvent;
		Windows::Foundation::EventRegistrationToken clickedEvent;
		bool alive;
		int deadtime;
		float speed;
		int initialHealth;
		int health;
};
