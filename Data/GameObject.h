#pragma once

using namespace Rapid2D;
using namespace Platform;

ref class GameObject
{
	protected:
		property Rapid2DGameObject ^ m_go;
	internal:
		GameObject(String ^ name, bool isDynamic, bool isPhysical, Rapid2DVector ^ position)
		{
			m_go = ref new Rapid2DGameObject(name, isDynamic, isPhysical, position);
			static int idInc = 0;
			myID = idInc++;
			destroyEvent = m_go->mEvents->OnObjectDestroy += ref new dgObjectDestroy(this, &GameObject::OnDestroy);		
		}

	private:
		Windows::Foundation::EventRegistrationToken destroyEvent;
		void GameObject::OnDestroy()
		{
			m_go->mEvents->OnObjectDestroy -= destroyEvent;	
			m_go->Cleanup();
			m_go = nullptr;
			UserMain::GetInstance()->ObjectDeleted(myID);
		}

	public:
		property int myID;

		Rapid2DGameObject ^ GetObject()
		{
			return m_go;
		}
		void SetGravity(float gravity)
		{
			m_go->SetGravity(gravity);
		}
		void AddForce (float x,float y, int _RotationCoordinate)
		{
			m_go->AddForce(x,y,_RotationCoordinate);
		}
		void AddForce(Rapid2DVector ^ _Direction, int _RotationCoordinate)
		{
			m_go->AddForce(_Direction, _RotationCoordinate);
		}
		void Translate(Rapid2DVector ^ Direction, int RotationCoordinate)
		{
			m_go->Translate(Direction, RotationCoordinate);
		}
		void Translate(float x,float y, int _RotationCoordinate)
		{
			m_go->Translate(x,y,_RotationCoordinate);
		}
		void TranslateVelocity(Rapid2DVector ^ Direction, int RotationCoordinate)
		{
			m_go->TranslateVelocity(Direction, RotationCoordinate);
		}
		void TranslateVelocity (float x,float y, int _RotationCoordinate)
		{
			m_go->TranslateVelocity(x,y,_RotationCoordinate);
		}
		void RotateToLookAt(Rapid2DVector ^ _LookAt)
		{
			m_go->RotateToLookAt(_LookAt);
		}
		void RotateToLookAt(float x,float y)
		{
			m_go->RotateToLookAt(x,y);
		}
		void RotateAroundALocalPoint(float Angle,Rapid2DVector ^ Point)
		{
			m_go->RotateAroundALocalPoint(Angle, Point);
		}
		void RotateAroundALocalPoint(float Angle, float x, float y)
		{
			m_go->RotateAroundALocalPoint(Angle, x, y);
		}
		void SetDrawScale(float x,float y)
		{
			m_go->SetDrawScale(x,y);
		}
		void SetDrawScale (Rapid2DVector ^ Scale)
		{
			m_go->SetDrawScale(Scale);
		}
		void SetCollisionScale(float x,float y)
		{
			m_go->SetCollisionScale(x,y);
		}
		void SetScale(float x,float y)
		{
			m_go->SetScale(x,y);
		}
		void SetCollisionGroup(String^ _Name)
		{
			m_go->SetCollisionGroup(_Name);
		}
		Rapid2DVector ^ GetMousePosition()
		{
			return m_go->GetMousePosition();
		}
		Rapid2DVector ^ GetMainCameraPosition()
		{
			return m_go->GetMainCameraPosition();
		}
		Rapid2DVector ^ GetGlobalPositionLocalOffset(Rapid2DVector ^ Point)
		{
			return m_go->GetGlobalPositionLocalOffset(Point);
		}
		Rapid2DVector ^ GetGlobalPositionLocalOffset(float x, float y)
		{
			return m_go->GetGlobalPositionLocalOffset(x,y);
		}
		void SetPosition(float x,float y)
		{
			m_go->SetPosition(x,y);
		}
		Rapid2DVector ^ GetVelocity()
		{
			return m_go->GetVelocity();
		}
		float GetVelocitySize()
		{
			return m_go->GetVelocitySize();
		}
		void SetVelocity(float x,float y)
		{
			m_go->SetVelocity(x,y);
		}
		Rapid2DVector ^ GetCollisionScale()
		{
			return m_go->GetCollisionScale();
		}
		Rapid2DVector ^ GetDrawScale()
		{
			return m_go->GetDrawScale();
		}	
		float GetRotationAngle()
		{
			return m_go->GetRotationAngle();
		}
		void Rotate(float angle)
		{
			m_go->Rotate(angle);
		}
		void SetRotation(float angle)
		{
			m_go->SetRotation(angle);
		}
		void FixRotation(bool flag)
		{
			m_go->FixRotation(flag);
		}
		void SetPhysicsBody(bool flag)
		{
			m_go->SetPhysicsBody(flag);
		}
		void SetDynamicBody(bool flag)
		{
			m_go->SetDynamicBody(flag);
		}	
		void SetTag(String ^ ChangeTo)
		{
			m_go->SetTag(ChangeTo);
		}
		void SetName(String ^ ChangeTo)
		{
			m_go->SetName(ChangeTo);
		}		
		String ^ GetName()
		{
			return m_go->GetName();
		}
		String ^ GetTag()
		{
			return m_go->GetTag();
		}
		bool RayCast(float _Angle, float _Length, String ^ Tag)
		{
			return m_go->RayCast(_Angle, _Length, Tag);
		}
		bool RayCast(Rapid2DVector ^ _StartPoint,float _Angle, float _Length, String ^ Tag)
		{
			return m_go->RayCast(_StartPoint, _Angle, _Length, Tag);
		}
		Rapid2DVector ^ GetWidthAndHeight()
		{
			return m_go->GetWidthAndHeight();
		}
		void SetCollisionToSphere(float _Radius)
		{
			m_go->SetCollisionToSphere(_Radius);
		}
		void SetCollisionToCustom(Windows::Foundation::Collections::IVector<Rapid2DVector ^> ^ vertices)
		{
			m_go->SetCollisionToCustom(vertices);
		}
		void SetFriction(float _Friction)
		{
			m_go->SetFriction(_Friction);
		}
		void SetDensity(float _Density)
		{
			m_go->SetDensity(_Density);
		}
		void IsTrigger(bool _Trigger)
		{
			m_go->IsTrigger(_Trigger);
		}
		bool IsTriggered()
		{
			return m_go->IsTriggered();
		}
		Rapid2DVector ^ GetPosition()
		{
			return m_go->GetPosition();
		}
		void AddTexture(String ^ m_TextureName, bool _IsSprite, int _Rows, int _Columns)
		{
			m_go->AddTexture(m_TextureName, _IsSprite, _Rows, _Columns);
		}		
		void SetTexture(String ^ m_TextureName)
		{
			m_go->SetTexture(m_TextureName);
		}
		void PlaySpriteTexture()
		{
			m_go->PlaySpriteTexture();
		}
		void PauseSpriteTexture()
		{
			m_go->PauseSpriteTexture();
		}
		void ResumeSpriteTexture()
		{
			m_go->ResumeSpriteTexture();
		}	
		void SetTextureFrame()
		{
			m_go->SetTextureFrame();
		}
		void SetTextureFrame(int _FrameNumber)
		{
			m_go->SetTextureFrame(_FrameNumber);
		}
		void SetAnimationSpeed(float _Speed)
		{
			m_go->SetAnimationSpeed(_Speed);
		}
		int GetCurrentTextureFrame()
		{
			return m_go->GetCurrentTextureFrame();
		}
		String ^ GetTextureName()
		{
			return m_go->GetTextureName();
		}
		bool IsPlayingTexture()
		{
			return m_go->IsPlayingTexture();
		}
		void SetFontColor(Rapid2DColor ^ _FontColor)
		{
			m_go->SetFontColor(_FontColor);
		}
		void SetSizeOfTextBlockInPixel(float _Width, float _Height)
		{
			m_go->SetSizeOfTextBlockInPixel(_Width, _Height);
		}
		void SetAlignmentTextBlock(int _Alignment)
		{
			m_go->SetAlignmentTextBlock(_Alignment);
		}
		void SetText(String ^ _Text)
		{
			m_go->SetText(_Text);
		}
		bool CompareTag(String ^ TestTo)
		{
			return m_go->CompareTag(TestTo);
		}
		bool CompareName(String ^ TestTo)
		{
			return m_go->CompareName(TestTo);
		}
		bool IsDynamic()
		{
			return m_go->IsDynamic();
		}
		bool IsPhysicsBody()
		{
			return m_go->IsPhysicsBody();
		}
		void AddSound(String ^ filename, int type)
		{
			m_go->AddSound(filename, type);
		}
		void PlaySound(String ^ soundname, bool isDirectional)
		{
			m_go->PlaySound(soundname, isDirectional);
		}
		void StopSound(String ^ soundname)
		{
			m_go->StopSound(soundname);
		}
		int GetLayerID()
		{
			return m_go->GetLayerID();
		}
		void Destroy()
		{
			m_go->Destroy();
		}
		void SetDamping(float Damping)
		{
			m_go->SetDamping(Damping);
		}
		void SetBouncy(float Bouncy)
		{
			m_go->SetBouncy(Bouncy);
		}
		void SetRender(bool SetTo)
		{
			m_go->SetRender(SetTo);
		}
		bool GetRender()
		{
			return m_go->GetRender();
		}
};