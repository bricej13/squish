#pragma once
#include "GameObject.h"

ref class ExampleObject sealed : public GameObject
{
	public:
		//These are just placeholders showing you how to claim and use the events.
		void OnCollide(Rapid2DGameObject ^ go);
		void OnStart();
		void OnUpdate(int64 frameNumber);
		void OnRender();
		void OnDestroy();
		void OnClicked();

	internal:

		ExampleObject(String ^ name, bool isDynamic, bool isPhysical, Rapid2DVector ^ position)
		: GameObject(name, isDynamic, isPhysical, position)
		{
			//Only add the events that you want to use, for better performance.
			startEvent = m_go->mEvents->OnStart += ref new dgObjectStart(this, &ExampleObject::OnStart);
			updateEvent = m_go->mEvents->OnUpdate+= ref new dgObjectUpdate(this, &ExampleObject::OnUpdate);
			renderEvent = m_go->mEvents->OnObjectRender += ref new dgObjectRender(this, &ExampleObject::OnRender);
			collideEvent = m_go->mEvents->OnObjectCollide += ref new dgObjectCollide(this, &ExampleObject::OnCollide);		
			destroyEvent = m_go->mEvents->OnObjectDestroy += ref new dgObjectDestroy(this, &ExampleObject::OnDestroy);		
			clickedEvent = m_go->mEvents->OnObjectClicked += ref new dgObjectClicked(this, &ExampleObject::OnClicked);
		};
	private:
		Windows::Foundation::EventRegistrationToken updateEvent;
		Windows::Foundation::EventRegistrationToken renderEvent;
		Windows::Foundation::EventRegistrationToken destroyEvent;
		Windows::Foundation::EventRegistrationToken collideEvent;
		Windows::Foundation::EventRegistrationToken startEvent;
		Windows::Foundation::EventRegistrationToken clickedEvent;
};
