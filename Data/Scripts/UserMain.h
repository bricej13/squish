#pragma once
#include "BasicMath.h"

using namespace Rapid2D;
using namespace Platform::Collections;
using namespace Platform;

/**
 * The UserMain is the sub-class of the Main class. It is the Highest level game-loop class to which the users have direct access. 
 * It hides all the Engine Level routines, like Layers, Physics, Rendering etc. 
 * Note: The Main class is the Mid-Level game-loop class that hides the Windows Metro Specific routines, integration of the Box2D physics engine, etc.
 * The UserMain is where the users are supposed handle their game-specifc logics, create there game-specific objects etc.
 */
namespace Rapid2D
{	
	ref class UserMain sealed
	{
		private:
			RenderMain ^ m_renderMain;
			static UserMain^ m_Instance;
			std::map<int, std::pair<Platform::String ^, Object ^>> objects;
			Object ^ bob;
			int lives;
			int score;
			bool paused;
			//vector<pair<Platform::String ^,pair<int, Object ^>>objectsName;

			Rapid2DLayer ^ BugLayer;
		public:
			
			static UserMain^ GetInstance()
			{
				return m_Instance;
			}
		
			
			void SetCameraPosition(Rapid2DVector ^ _TouchPoint);
			void Add(Object ^ ob, String ^ name, int id);
			Object ^ Get(int id);
			Object ^ Get(Platform::String ^ name);
			int GetLives() { return lives; };
			void SetLives(int i) { lives = i; } ;
			void DecLives(int i) { lives += i;} ;
			int GetScore() { return score; }
			void SetScore(int s) { score = s; }
			void IncScore(int s) { score += s; }
			void Pause();
			UserMain()
			{
				m_Instance = this;
				m_renderMain = ref new RenderMain();
				m_renderMain->OnStart += ref new dgStart(this, &UserMain::Start);
				m_renderMain->OnUpdate += ref new dgUpdate(this, &UserMain::Update);
				m_renderMain->OnPointerReleased += ref new dgOnPointerReleased(this, &UserMain::PointerReleased);
				m_renderMain->OnPointerPressed += ref new dgOnPointerPressed(this, &UserMain::PointerPressed);
				m_renderMain->OnPointerMoved += ref new dgOnPointerMoved(this, &UserMain::PointerMoved);
				m_renderMain->Init();
				score = 0;
			}

			void ObjectDeleted(int id)
			{
				std::map <int, std::pair<Platform::String ^, Object ^>> :: const_iterator Iter;
				Iter=objects.find(id);
				if (Iter != objects.end( ))  objects.erase (Iter);
			}

			/**
			 * The engine calles the Start function at the start of the game. 
			 * This is where all the game related initialization should be done.
			 */
			virtual void Start();

			/**
			 * The Update function on the UserMain is called by the Engine each frame.
			 * This is where your game loop should exist.
			 *
			 * @param		frameNumber			The value of the current frame
			 */
			virtual void Update(int64 frameNumber);

			/**
			 * This is called by the Controller class when it receives a PointerPress event.
			 * A pointer press event is fired when either a Tap or click occurs in the game.
			 *
			 * @param		_Point			The point in the game world where the event took place
			 */
			virtual void PointerPressed(Rapid2DVector ^ _Point);

			/**
			 * This is called by the Controller class when it receives a PointerMoved event.
			 * A pointer moved event is fired when either a 'Tap and drag' or 'Click and Drag' takes place.
			 *
			 * @param		_Point			The current pointer position while in the pointer moved event
			 */
			virtual void PointerMoved(Rapid2DVector ^_Point);

			/**
			 * This is called by the Controller class when it receives a PointerReleased event.
			 * A pointer released event is fired when either an UnTap or click-released occurs in the game.
			 *
			 * @param		_Point			The point in the game world where the event took place
			 */
			virtual void PointerReleased(Rapid2DVector ^_Point);


			void AddBug(int health);
	};
}

