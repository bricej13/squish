#include "../pch.h"
#include "UserMain.h"
#include "../BasicMath.h"
#include "BugLady.h"
#include "ExampleObject.h"
#include "UserMain.h"
// CAUTION -- This is a generated class, this is built by the Rapid2D editor, changing this file could cause files to not be loaded
using namespace std;
class LoadFile
{
public:
RenderMain ^ m_renderMain;
UserMain ^ UM;
LoadFile(	RenderMain ^ _renderMain,UserMain ^ _UM){m_renderMain=_renderMain;UM=_UM;
loadfirst(L"FirstScene");}
void loadfirst(wstring _first){
Rapid2DLayer ^ store;
m_renderMain->AddCollisionGroup("Default",1,65535);
	 if(_first.compare(L"FirstScene") == 0){
			store=m_renderMain->AddLayer();
			GameObject^ op0 = ref new GameObject(ref new Platform::String(L"BG Object"),false,true, ref new Rapid2DVector(-0.000000f,0.000000f));
			op0->SetRotation(0.000000f);
			op0->SetDensity(1.00000f);
			op0->SetFriction(0.800000f);
			op0->SetBouncy(0.000000f);
			op0->SetDamping(0.000000f);
			op0->SetTag("");
			op0->SetCollisionScale(1.00000f,1.00000);
			op0->SetCollisionGroup("Default");
			op0->AddTexture("background.jpg",false,1,1);
			op0->SetTexture("background.jpg");
			op0->SetScale(1.00000f,1.00000f);
			store->AddObjectToLayer(op0->GetObject());
			UM->Add(op0, op0->GetName(),op0->myID);
			store=m_renderMain->AddLayer();
			BugLady^ op1 = ref new BugLady(ref new Platform::String(L"bug_lady"),false,true, ref new Rapid2DVector(0.800000f,-0.0400000f));
			op1->SetRotation(120.000f);
			op1->SetDensity(1.00000f);
			op1->SetFriction(0.800000f);
			op1->SetBouncy(0.000000f);
			op1->SetDamping(0.000000f);
			op1->SetTag("");
			op1->SetCollisionScale(1.00000f,1.00000);
			op1->SetCollisionGroup("Default");
			op1->AddTexture("bug_lady.png",false,1,1);
			op1->SetTexture("bug_lady.png");
			op1->SetScale(0.200000f,0.200000f);
			//store->AddObjectToLayer(op1->GetObject());
			//UM->Add(op1, op1->GetName(),op1->myID);
}
}
};