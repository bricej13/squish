#include "pch.h"
#include "UserMain.h"
#include "LoadFile.h"
#include "wtypes.h"
#include <iostream>
#include <ctime> 

using namespace Rapid2D;
using namespace Platform;
using namespace Platform::Collections;
using namespace std;

UserMain^ UserMain::m_Instance;

void UserMain::Add(Object ^ ob, String ^ name, int id)
{
	objects.insert(std::make_pair(id, std::make_pair(name,ob)));
}
Object ^ UserMain::Get(int id)
{
    map <int, std::pair<Platform::String ^, Object ^>> :: const_iterator Iter;
	Iter=objects.find(id);
	if (Iter == objects.end( )) return nullptr;
	return (Iter->second.second);
}

Object ^ UserMain::Get(String ^ name)
{
	  map <int, std::pair<Platform::String ^, Object ^>> :: const_iterator Iter;
	  for (Iter = objects.begin(); Iter != objects.end(); Iter++) {
		  if( name==  Iter->second.first)
		  {
			  	return (Iter->second.second);
		  }
      }
	  return nullptr;
}

void UserMain::SetCameraPosition(Rapid2DVector ^ _Point)
{
	m_renderMain->SetCameraPosition(_Point);
}
void UserMain::Start()
{
	SetLives(10);
	LoadFile *f =new LoadFile(m_renderMain,this);
	BugLayer = m_renderMain->AddLayer();
	#pragma warning(disable:4244)
	srand( time(0) );
	#pragma warning(default:4244)
	AddBug(1);
	AddBug(2);
	AddBug(3);
}

void UserMain::Update(int64 frameNumber)
{

}

void UserMain::PointerPressed(Rapid2DVector ^ _TouchPoint)
{

}

void UserMain::PointerMoved(Rapid2DVector ^ _TouchPoint)
{


}

void UserMain::PointerReleased(Rapid2DVector ^ _TouchPoint)
{

}

void UserMain::AddBug(int health) {
	float new_y = rand() % 20 - 10;
	BugLady^ new_bug = ref new BugLady(ref new Platform::String(L"bug_lady"),false,true, ref new Rapid2DVector(10.000000f,new_y));
	new_bug->SetRotation(120.0000f);
	new_bug->SetDensity(1.00000f);
	new_bug->SetFriction(0.800000f);
	new_bug->SetBouncy(0.000000f);
	new_bug->SetDamping(0.000000f);
	new_bug->SetTag("");
	new_bug->SetCollisionScale(1.00000f,1.00000);
	new_bug->SetCollisionGroup("Default");
	new_bug->AddTexture("bug_lady.png",false,1,1);
	new_bug->SetTexture("bug_lady.png");
	new_bug->SetScale(0.200000f,0.200000f);
	new_bug->SetHealth(health);
	BugLayer->AddObjectToLayer(new_bug->GetObject());
	Add(new_bug, new_bug->GetName(), new_bug->myID);
}


