#include "pch.h"
#include "BugLady.h"
void BugLady::OnCollide(Rapid2DGameObject ^j){
}
void BugLady::OnStart()
{
	if (m_go==nullptr) return;
	SetGravity(0);
	//this->speed = -1*((float)rand()/((float)RAND_MAX)/10);
	this->speed = -1 * ((float)(rand()%3+4))/100;

}
void BugLady::OnDestroy()
{
	if (m_go==nullptr) return;
	m_go->mEvents->OnStart -= startEvent;
	m_go->mEvents->OnUpdate -= updateEvent;
	m_go->mEvents->OnObjectRender -= renderEvent;
	m_go->mEvents->OnObjectCollide -= collideEvent;
	m_go->mEvents->OnObjectDestroy -= destroyEvent;
	m_go->mEvents->OnObjectClicked -= clickedEvent;
}
void BugLady::OnClicked()
{
	// hurt bug if still alive
	if (this->health > 0) {
		this->DecHealth(1);
		// the killing blow
		if (this->health <= 0) {
			this->AddTexture("splat.png",false,1,1);
			this->SetTexture("splat.png");
			this->deadtime = 100;
			PlaySound(TEXT("squish.wav"), NULL);
			UserMain::GetInstance()->IncScore(this->initialHealth);
		}
		else {
			PlaySound(TEXT("hit.wav"), NULL);
		}
	}
	
}
void BugLady::OnUpdate(int64 frameNumber)
{
	Rapid2DVector ^ pos = this->GetPosition();

	if (this->health > 0){
		if (pos->x < -10){
			UserMain::GetInstance()->DecLives(1);
			ResetBug();
		}
		else {
			Translate(this->speed,0.000000f,1);
		}
	}
	else if (this->deadtime > 0) {
		this->deadtime--;
	}
	else {
		ResetBug();
		return;
	}
}
void BugLady::OnRender()
{
	if (m_go==nullptr) return;
}

void BugLady::ResetBug() {
	float new_y = rand() % 20 - 10;
	this->SetPosition(10.000000f,new_y);

	//this->speed = -1*((float)rand()/((float)RAND_MAX)/10);
	this->speed = -1 * ((float)(rand()%3+4))/100;
	this->deadtime = 0;
	this->health = this->initialHealth;

	this->AddTexture("bug_lady.png",false,1,1);
	this->SetTexture("bug_lady.png");
}

void BugLady::SetHealth(int h) {
	this->initialHealth = h;
	this->health = h;
}

void BugLady::DecHealth(int dmg) {
	this->health = this->health - dmg;
}
