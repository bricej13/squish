#include "pch.h"
#include "ExampleObject.h"

void ExampleObject::OnCollide(Rapid2DGameObject ^j){
}
void ExampleObject::OnStart()
{
	if (m_go==nullptr) return;

}
void ExampleObject::OnDestroy()
{
	if (m_go==nullptr) return;
	m_go->mEvents->OnStart -= startEvent;
	m_go->mEvents->OnUpdate -= updateEvent;
	m_go->mEvents->OnObjectRender -= renderEvent;
	m_go->mEvents->OnObjectCollide -= collideEvent;		
	m_go->mEvents->OnObjectDestroy -= destroyEvent;	
	m_go->mEvents->OnObjectClicked -= clickedEvent;
}
void ExampleObject::OnClicked()
{

}
void ExampleObject::OnUpdate(int64 frameNumber)
{

}
void ExampleObject::OnRender()
{
	if (m_go==nullptr) return;
}